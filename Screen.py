from Genetics import Population
from Simulator import Road, RoudRunner1
import gc
import Logger

count = 0
NUM_OF_CARS = 10
STOP_CONDITION = 7
timeDifference = 0

def calculateDiffrenceTime(result, prvBestCar):
    global timeDifference
    for carData in result:
        if carData.isEqual(prvBestCar.data):
            timeDifference = carData.timeRun - prvBestCar.data.timeRun
    print(timeDifference)

def madeChangesInTime(result):
    global timeDifference
    for carData in result:
        carData.timeRun = carData.timeRun - timeDifference

def isUltimate():
    global count
    if (count == STOP_CONDITION):
        return True
    return False

def goingToBeUltimate(bestCar, prevBestCar):
    global count
    if (bestCar.isSame(prevBestCar)):
        count += 1
    else:
        count = 0

def main():
    print("screen")
    road = Road.Road("Simulator/RoadFile.txt")
    population = Population.Population(NUM_OF_CARS)    # create random population
    population.bestCar = population.cars[0]
    population.prevBestCar = population.cars[0]
    population.mutateTheBabies()

    logger = Logger.Logger()

    reachUltimateCar =  False

    while not reachUltimateCar:
        gc.collect()
        gc.garbage.clear()
        # check the population on the road and get results

        result = RoudRunner1.run(population.gen, population.cars, road)
        logger.writeToLog(result)


        # Take care about the end of genetics/program:
        population.prevBestCar = population.bestCar
        population.calculateFitness()
        population.setBestCar()


        if (population.gen - 1 >= 1):
            calculateDiffrenceTime(result, population.prevBestCar) 

            goingToBeUltimate(population.bestCar, population.prevBestCar)
            reachUltimateCar = isUltimate()

        if (not reachUltimateCar):
            # uses the result for make new population
            population.naturalSelection()
            population.mutateTheBabies()


        
    print ("I was finished")
    print(population.bestCar.data)
    RoudRunner1.window.close()

if __name__ == "__main__":
    main()

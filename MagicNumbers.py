from enum import Enum
class IDS(Enum):
    END_ROAD = 1
    BACK_WHEEL = 2
    FRONT_WHEEL = 3
    NORMAL_ROAD = 15
    CAR_CHASSIS = 100
    MIDDLE_FRAME = 450

from Genetics import CarData

class Logger():
    def __init__(self):
        # Data car got
        self.path = "Results"
        self.gen = 1


    def writeToLog(self, result):
        lines = []
        for resCar in result:
            lines.append(resCar.to_string())
        with open(self.path +".txt",  "a") as file:
            file.write("generation" + self.gen.__str__() + '\n')
            for line in lines:
                file.write(line + '\n')
            file.write('\n')

        self.gen += 1


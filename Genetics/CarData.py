class CarData():
    def __init__(self, cMass, fMass, fRadius, bMass, bRadius, color):
        # Data car got
        self.cMass = cMass
        self.fMass = fMass
        self.fRadius = fRadius
        self.bMass = bMass
        self.bRadius = bRadius
        self.color = color

        # Her performance:
        self.distanceX = 0
        self.timeRun = 0
        self.isFinished = False



    def isEqual(self, CarData2):
        if (self.cMass == CarData2.cMass and  self.fMass == CarData2.fMass and self.fRadius == CarData2.fRadius and self.bMass == CarData2.bMass and self.bRadius == CarData2.bRadius):
            return True
        return False

    def to_string(self):
        cmass= "Chasis Mass = " + self.cMass.__str__()
        fMass= "Front Wheel Mass = " + self.fMass.__str__()
        fRadius= "Front Wheel Radius = " + self.fRadius.__str__()
        bMass= "Back Wheel Mass = " + self.bMass.__str__()
        bRadius= "Back Wheel Radius = " + self.bRadius.__str__()
        data = cmass + '    ' + fMass + '    ' + fRadius + '    ' + bMass + '    ' + bRadius

        distanceX = "distance on X = " + self.distanceX.__str__()
        timeRun = "time Run = " + self.timeRun.__str__()
        isFinished = "is Finished = " + self.isFinished.__str__()

        performance = distanceX + '     ' + timeRun + '     ' + isFinished

        return "data: " + data + "  ------> results:" + performance

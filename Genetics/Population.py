from Genetics import Car
import random

class Population:
    fitnessSum = 0.0
    gen = 1
    bestCar = None
    prevBestCar = None
    cars = []

    def __init__(self, size):
        for i in range(size):
            self.cars.append(Car.Car())

    def calculateFitness(self):
        for car in self.cars:
            car.calculateFitness()


    def calculateFitnessSum(self):
        self.fitnessSum = 0.0
        for car in self.cars:
            self.fitnessSum += car.fitness

    def selectParent(self):
        rand = random.random() * self.fitnessSum
        runningSum = 0.0

        for pCar in self.cars:
            runningSum += pCar.fitness
            if (runningSum > rand):
                return pCar

        # should never get to this point
        return None #needs to return None


    def mutateTheBabies(self):
        for car in self.cars:
            if car != self.bestCar:
                car.brain.mutate()
                while (car.brain.isEqual(self.bestCar.brain)):
                    car.brain.mutate()


    def setBestCar(self): # Set best car by fitness (by the distance and time)
        max = 0.0
        for aCar in self.cars:
            if aCar.fitness > max:
                max = aCar.fitness
                self.bestCar = aCar

    def naturalSelection(self):
        newCars = []
        self.setBestCar()
        self.calculateFitnessSum()

        # the champion lives on
        newCars.append(self.bestCar.giveBaby())
        newCars[0].bestCar = True

        for i in range(len(self.cars) - 1):
            # select parent based on fitness
            parent = self.selectParent()

            # get baby from them
            newCars.append(parent.giveBaby())

        self.bestCar = newCars[0]
        self.cars = newCars[:]
        self.gen += 1





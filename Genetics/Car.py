import pymunk
from MagicNumbers import IDS
import datetime
from Genetics import Brain, CarData


class Car:
    def __init__(self):
        self.brain = Brain.Brain()
        self.fitness = 0.0
        self.finishedRace = False
        self.bestCar = False

        self.lastDistanceX = 0
        self.lastX = 0
        self.timeLastChange = datetime.datetime.now()
        self.distanceX = 0

    def createCar(self):
        car_filter = pymunk.ShapeFilter(group=1)

        self.data = CarData.CarData(self.brain.cMass, self.brain.fMass, self.brain.fRadius, self.brain.bMass, self.brain.bRadius, self.brain.color)

        self.bWheel_body = pymunk.Body(self.brain.bMass, pymunk.moment_for_circle(self.brain.bMass, 0, self.brain.bRadius))
        self.bWheel_shape = pymunk.Circle(self.bWheel_body, self.brain.bRadius)
        self.bWheel_shape.friction = 0.62
        self.bWheel_shape.filter = car_filter
        self.bWheel_shape.color = (134, 134, 134, 100)
        self.bWheel_shape.id = IDS.BACK_WHEEL
        self.bWheel_shape.color = self.brain.color

        self.fWheel_body = pymunk.Body(self.brain.fMass, pymunk.moment_for_circle(self.brain.fMass, 0, self.brain.fRadius))
        self.fWheel_shape = pymunk.Circle(self.fWheel_body, self.brain.fRadius)
        self.fWheel_shape.friction = 0.62
        self.fWheel_shape.filter = car_filter
        self.fWheel_shape.color = (134, 134, 134, 100)
        self.fWheel_shape.id = IDS.FRONT_WHEEL
        self.fWheel_shape.color = self.brain.color

        self.chassis_shape = pymunk.Poly.create_box(None, (self.brain.cWidth, self.brain.cHeight))
        self.chassis_body = pymunk.Body(self.brain.cMass, pymunk.moment_for_poly(self.brain.cMass, self.chassis_shape.get_vertices()))
        self.chassis_shape.body = self.chassis_body
        self.chassis_shape.filter = car_filter
        self.chassis_shape.friction = 0.9
        if self.bestCar:
            self.chassis_shape.color = (220, 162, 0)
        else:
            self.chassis_shape.color = (255 - self.brain.cHeight, 255 - self.brain.cHeight, 255 - self.brain.cHeight, 10)
        self.chassis_shape.id = IDS.NORMAL_ROAD

        self.bMotor = pymunk.SimpleMotor(self.bWheel_body, self.chassis_body, 0)
        self.fMotor = pymunk.SimpleMotor(self.fWheel_body, self.chassis_body, 0)

        self.chassis_body.position = self.brain.cPos
        self.bWheel_body.position = (self.brain.cPos[0] - (self.brain.cWidth / 2.5), self.brain.cPos[1] / 1.05)
        self.fWheel_body.position = (self.brain.cPos[0] + (self.brain.cWidth / 2.5), self.brain.cPos[1] / 1.05)

        self.bJoint = pymunk.SlideJoint(self.bWheel_body, self.chassis_body, (0, 0), (-(self.brain.cWidth / 2.2), -self.brain.cHeight / 1.2), 0, 0)
        self.fJoint = pymunk.SlideJoint(self.fWheel_body, self.chassis_body, (0, 0), (self.brain.cWidth / 2.2, -self.brain.cHeight / 1.2), 0, 0)

    def add_to_space(self, space):
        space.add(self.bWheel_body, self.bWheel_shape, self.fWheel_body, self.fWheel_shape, self.chassis_body, self.chassis_shape, self.bMotor, self.fMotor, self.bJoint,  self.fJoint)

    def remove_from_space(self, space):
        space.remove(self.bWheel_body, self.bWheel_shape, self.fWheel_body, self.fWheel_shape, self.chassis_body, self.chassis_shape, self.bMotor, self.fMotor, self.bJoint, self.fJoint)

    def set_speed(self, speed):
        self.bMotor.rate = speed
        self.fMotor.rate = speed


    def set_friction(self, fric):
        self.fWheel_shape.friction = fric
        self.bWheel_shape.friction = fric

    def getCarLocation(self):
        return self.chassis_body.position

    def CarGoOn(self, carRunNow):
        if (carRunNow > 1):
            self.timeLastChange = datetime.datetime.now()
        self.lastDistanceX = self.distanceX
        self.distanceX += carRunNow

    def getDistanceCarMade(self):
        return abs(self.distanceX)


    def insertPerformanceToCarData(self, isFinished, StartTime):
        self.data.isFinished = isFinished
        self.data.distanceX = self.distanceX
        self.data.timeRun = datetime.datetime.now() - StartTime

    def calculateFitness(self):
        if self.finishedRace:
            self.fitness = 1.0 + (self.getDistanceCarMade())/(self.data.timeRun.total_seconds() * self.data.timeRun.total_seconds())
        else:
            try:
                distanceToEnd = 15000 - self.getDistanceCarMade()
                self.fitness = 1.0/(distanceToEnd * distanceToEnd)
            except:
                self.fitness = 0.00000000000001


    def giveBaby(self):
        baby = Car()
        baby.brain = self.brain.clone()
        return baby

    def isSame(self, car2):
        return (self.finishedRace and self.data.isEqual(car2.data))


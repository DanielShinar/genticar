import random

class Brain:
    def __init__(self):
        brainParts = []
        with open("Genetics/basicCar.txt") as f:
            content = f.readlines()
        for i in content:
            brainParts.append(eval(i))

        self.cMass = brainParts[0]
        self.cWidth = brainParts[1]
        self.cHeight = brainParts[2]
        self.fMass = brainParts[3]
        self.fRadius = brainParts[4]
        self.bMass = brainParts[5]
        self.bRadius = brainParts[6]
        self.cPos = (150, 100)
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

    def clone(self): # like copy constructor
        clone = Brain()
        clone.cMass = self.cMass
        clone.cWidth = self.cWidth
        clone.cHeight = self.cHeight
        clone.fMass = self.fMass
        clone.fRadius = self.fRadius
        clone.bMass = self.bMass
        clone.bRadius = self.bRadius
        clone.cPos = self.cPos
        clone.color = self.color

        return clone

    def mutate(self):
        mutationRate = 50 #25% chance to mutate
        for i in range(1):
            chanceOfMtation = random.randint(0, 100)
            if i == 0:
                if mutationRate >= chanceOfMtation:
                    self.bRadius = int((random.randint(0, 70)))

    def load(self, _cMass, _cWidth, _cHeight, _fMass, _fRadius, _bMass, _bRadius, _color):
        self.cMass = _cMass
        self.cWidth = _cWidth
        self.cHeight = _cHeight
        self.fMass = _fMass
        self.fRadius = _fRadius
        self.bMass = _bMass
        self.bRadius = _bRadius
        self.color = _color

    def isEqual(self, brain2):
        if (self.cMass == brain2.cMass and self.cWidth == brain2.cWidth and self.cHeight == brain2.cHeight and self.fMass == brain2.fMass and self.fRadius == brain2.fRadius and self.bMass == brain2.bMass and self.bRadius == brain2.bRadius):
            return True
        return False





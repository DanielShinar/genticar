import pymunk, pyglet, time
from pymunk.pyglet_util import DrawOptions
from math import degrees
import random
from MagicNumbers import IDS
import math

class Road:
    def __init__(self, path):
        self.path = path
        self.roadLst = self.readARoud()
        self.roadLength = self.roadLst[len(self.roadLst)-1][0] - self.roadLst[0][0]
        self.roadAngles = []

    def readARoud(self):
        roadLst = []
        with open(self.path) as f:
            content = f.readlines()
        for x in content:
            roadLst.append(eval(x))
        return roadLst

    def CalculateAngle(self, src, dist):
        myradians = math.atan2(src[1] - dist[1], src[0] - dist[0])
        return math.degrees(myradians)

    def CalculateAnglesRoad(self):
        if (len(self.roadAngles) + 1 != len(self.roadLst)):
            i = 0
            for i in range(len(self.roadLst)-1):
                if (i > 0):
                    self.roadAngles.append(self.CalculateAngle(self.roadLst[i-1], self.roadLst[i]))
            self.roadAngles.append(self.CalculateAngle(self.roadLst[i], self.roadLst[i+1]))

    def make_road(self, space):
        for i in range(len(self.roadLst)-1):
            if(i >= len(self.roadLst)-3):
                partBody = pymunk.Body(1, 1666, pymunk.Body.KINEMATIC)
                part_shape = pymunk.Segment(partBody, self.roadLst[i], self.roadLst[i + 1], 2)
                part_shape.body.position = 0, 0  # Set the position of the body
                part_shape.elasticity = 0.62
                part_shape.friction = 0.7
                part_shape.id = IDS.END_ROAD # last one
            else:
                partBody = pymunk.Body(1, 1666, pymunk.Body.KINEMATIC)
                part_shape = pymunk.Segment(partBody, (self.roadLst[i][0], self.roadLst[i][1]), (self.roadLst[i+1][0], self.roadLst[i+1][1]), 2)
                part_shape.body.position = 0, 0      # Set the position of the body
                part_shape.elasticity = 0.62
                part_shape.friction = 0.7
                part_shape.id = IDS.NORMAL_ROAD
            space.add(part_shape)



    def make_random_road(self, space, size, posOne, run, amountOfRoadParts):
        print(posOne)
        if(run == amountOfRoadParts):
            partBody = pymunk.Body(1, 1666, pymunk.Body.KINEMATIC)
            part_shape = pymunk.Segment(partBody, (0, 150), (150, 150), 2)
            part_shape.body.position = 0, 0      # Set the position of the body
            part_shape.elasticity = 0.62
            part_shape.friction = 0.62
            part_shape.id = 15
            space.add(part_shape)
            self.make_random_road(space, size, (150, 150), run-1,amountOfRoadParts)
            return
        elif(run > 0):
            modifier = random.randint(-80,80)
            while(posOne[1] + modifier < 0 or posOne[1] + modifier > 720-100):
                modifier = random.randint(-80,80)
            partBody = pymunk.Body(1, 1666, pymunk.Body.KINEMATIC)
            part_shape = pymunk.Segment(partBody, posOne, (posOne[0] + size, posOne[1] + modifier), 2)
            part_shape.elasticity = 0.62
            part_shape.friction = 0.62
            part_shape.id = 15
            space.add(part_shape)
            self.make_random_road(space, size, (posOne[0] + size, posOne[1] + modifier), run - 1, amountOfRoadParts)
            return
        else:
            return

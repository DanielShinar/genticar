import pymunk, pyglet
from pymunk.pyglet_util import DrawOptions
from Simulator.Road import *
import datetime
import time
from MagicNumbers import IDS
from pyglet.gl import *


# create window for the simulator
window = pyglet.window.Window(1280, 720, "Geni #1", resizable=True)
glScalef(0.085, 0.085, 0.085)

options = DrawOptions()

# create space in the window
space = pymunk.Space()
space.gravity = 0, -1000

# Global Variables
allCars = []
finishCars = []
output = []

StartTime = datetime.datetime.now()
TimeToStop = datetime.datetime.now()


onesGetEnd = False
gen = 0



# screen events:
def clear_all():
    global space
    for shape in space.shapes:
        space.remove(shape)
def timer(): # put timer at the top of the screen
    currentTime = datetime.datetime.now()
    timeRun = currentTime - StartTime
    msg = "Running time: " + str(timeRun) + " \n"
    label = (pyglet.text.Label(msg, font_name='Times New Roman', font_size=12, x=20, y = 680 , color=(255, 255, 255,255)))
    label.draw()
def initGlobals():
    global allCars
    global gen
    global StartTime
    global finishCars
    global output
    global stop
    global onesGetEnd

    # Global Variables
    allCars = []
    finishCars = []
    output = []


    StartTime = datetime.datetime.now()
    stop = False
    onesGetEnd = False


# windows events:
@window.event
def on_close():
    pyglet.clock.unschedule(update)
    clear_all()
    pyglet.app.exit()
@window.event
def on_draw():
    if (len(allCars) != 0):
        window.clear()
        space.debug_draw(options)
        timer()



# car events:
def addCar(car):
    allCars.append(car)
    car.timeLastChange = datetime.datetime.now()
    car.createCar()
def removeCar(car, isFinished):
    global finishCars
    global allCars

    allCars.remove(car)

    if (isFinished == True): # reach end
        finishCars.append(car)
    else:
        car.remove_from_space(space)

def addCarsToSpace():
    global allCars
    for car in allCars:
        car.add_to_space(space)
        car.lastX = car.getCarLocation()[0]
        car.set_speed(-40)


# simulator events:
def reachEnd():
    global onesGetEnd
    for car in allCars:
        if(car.getDistanceCarMade() > 14600):
            if len(finishCars) == 0:
                car.bestCar = True
            car.set_speed(0)
            onesGetEnd = True
            carStuck(car, True)
            car.finishedRace = True

def stopBecuseTimeOver(car, isFinished = False):
    global onesGetEnd
    global TimeToStop

    if onesGetEnd == True:
        timeAfterFirstCarFinish = (datetime.datetime.now() - TimeToStop)
        if (timeAfterFirstCarFinish.total_seconds() > 20.0):
            car.insertPerformanceToCarData(isFinished, StartTime) # Save Details
            output.append(car.data)
            removeCar(car,isFinished)
    else:
        TimeToStop = datetime.datetime.now()
def carStuck(car, isFinished = False):
    runTime = datetime.datetime.now() - StartTime

    if (isFinished == True): # reach end
            timeStuck = 21.0
    else:
        currentTime = datetime.datetime.now()
        timeStuck = (currentTime - car.timeLastChange).total_seconds()

    if(int(timeStuck) > 7.0):
        car.insertPerformanceToCarData(isFinished, StartTime) # Save Details
        removeCar(car, isFinished)
        output.append(car.data)

def update(dt):
    if (len(allCars) == 0): # all the population are dead
        print("all deads")
        pyglet.clock.unschedule(update)
        time.sleep(3)
        clear_all()
        pyglet.app.exit()
        return

    for car in allCars:
        carRun = int(car.getCarLocation()[0]) - car.lastX
        car.CarGoOn(carRun)
        # if should to delete car
        #stopBecuseTimeOver(car)
        carStuck(car)

    for car in allCars:  # save carX after the changed in location
        car.lastX = int(car.getCarLocation()[0])

    reachEnd()

    space.step(0.02)



def run(generation, Cars, road): # the "main" of the road runner
    global allCars
    global gen
    global StartTime

    initGlobals()


    # GENERATION:
    gen = generation

    # ROAD - put  a road in the space
    road.make_road(space)


    #CARS:
    for car in Cars:
        addCar(car)
    addCarsToSpace()


    #RUN
    print("start --- " + str(generation))
    StartTime = datetime.datetime.now()
    pyglet.clock.schedule_interval(update, 1.0/150)
    pyglet.app.run()

    return output


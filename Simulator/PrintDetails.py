import pymunk, pyglet
import datetime
from MagicNumbers import IDS


labels = []
Detailswindow = None


@Detailswindow.event
def on_draw():
    Detailswindow.clear()
    for label in labels:
        label.draw()
def makeAFrame(space):
    frame_body1 = pymunk.Body(1, 1666, pymunk.Body.KINEMATIC)
    frame_shape1 = pymunk.Segment(frame_body1, (5, 715), (500, 715), 2)
    frame_shape1.id = IDS.FRAME_ID
    space.add(frame_shape1)


    frame_body2 = pymunk.Body(1, 1666, pymunk.Body.KINEMATIC)
    frame_shape2 = pymunk.Segment(frame_body2, (5, 570), (500, 570), 2)
    frame_shape2.id = IDS.FRAME_ID
    space.add(frame_shape2)


    frame_body3 = pymunk.Body(1, 1666, pymunk.Body.KINEMATIC)
    frame_shape3 = pymunk.Segment(frame_body3, (5, 715), (5, 570), 2)
    frame_shape3.id = IDS.FRAME_ID
    space.add(frame_shape3)



def showCarDetails(leadingCar):
    X = 320
    Y = 680

    msg = "Leading Car Details: \n"
    labels.append(pyglet.text.Label(msg, font_name='Times New Roman', font_size=12, x=X, y =Y, color=(255, 255, 255,255)))
    Y -= 20


def showDetails(dt, Generation, StartTime, carsAlive, leadingCar):
    Detailswindow = pyglet.window.Window(580, 720, "Details", resizable=False)

    labels.clear()
    X = 20
    Y = 680

    msg = "Generation: " + str(Generation) + " \n"
    labels.append(pyglet.text.Label(msg, font_name='Times New Roman', font_size=12, x=X, y =Y, color=(255, 255, 255,255)))
    Y -= 20


    msg = "Cars Alive: " + str(carsAlive) + " \n"
    labels.append(pyglet.text.Label(msg, font_name='Times New Roman', font_size=12, x=X, y =Y, color=(255, 255, 255,255)))
    Y -= 20


    currentTime = datetime.datetime.now()
    timeRun = currentTime - StartTime
    msg = "Running time: " + str(timeRun) + " \n"
    labels.append(pyglet.text.Label(msg, font_name='Times New Roman', font_size=12, x=X, y =Y, color=(255, 255, 255,255)))
    Y -= 20


    distance = leadingCar.getDistanceCarMade() / 100
    msg = "Distance: " + str(distance) + " meters \n"
    labels.append(pyglet.text.Label(msg, font_name='Times New Roman', font_size=12, x=X, y =Y, color=(255, 255, 255,255)))
    Y -= 20


    height = "?????"
    msg = "Height: " + str(height) + " meters \n"
    labels.append(pyglet.text.Label(msg, font_name='Times New Roman', font_size=12, x=X, y =Y, color=(255, 255, 255,255)))
    Y -= 20

    msg = "The Leading Car Number is: " + str(leadingCar.id) +" \n"
    labels.append(pyglet.text.Label(msg, font_name='Times New Roman', font_size=12, x=X, y =Y, color=(255, 255, 255,255)))
    Y -= 20

    showCarDetails(leadingCar)

